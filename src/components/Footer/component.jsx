import React from "react";
import { NavLink } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Logo_footer from "@Assets/images/Logo_footer.png";

export default function Footer(props) {
  return (
    <footer className="footer">
      <div className="container">
        <div className="row">
          <div className="col-lg-3 col-md-6 col-sm-6">
            <div className="footer__about">
              <div className="footer__logo">
                <a href="./index.html">
                  <img src={Logo_footer} alt="" />
                </a>
              </div>
            </div>
          </div>
          <div className="col-lg-2 col-md-3 col-sm-6">
            <div className="footer__widget">
              <div className="label">ABOUT</div>
              <div className="footer__widget">
                <ul className="nav-glink">
                  <li>
                    <NavLink to="/about-us">About Us</NavLink>
                  </li>
                  <li>
                    <NavLink to="/faq">FAQ's</NavLink>
                  </li>
                  <li>
                    <NavLink to="/why-will">Why Will & More</NavLink>
                  </li>
                  <li>
                    <NavLink to="/price">Pricing</NavLink>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-md-3 col-sm-6">
            <div className="footer__widget">
              <div className="label">&nbsp;</div>
              <div className="footer__widget footer__widget--social">
                <ul className="nav-glink">
                  <li>
                    <NavLink to="/write-will">Write a Will</NavLink>
                  </li>
                  <li>
                    <NavLink to="/location-registration">
                      Will Location Registration
                    </NavLink>
                  </li>
                  <li>
                    <NavLink to="/letterof">Letter of Instructions</NavLink>
                  </li>
                  <li>
                    <NavLink to="/location-search">
                      Will Location Search
                    </NavLink>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 col-sm-6">
            <div className="footer__widget footer__widget--address">
              <div className="label">CONTACT US</div>
              <p>
                <FontAwesomeIcon icon="map-marker" /> Office No 1111, Maker
                Chamber V Nariman Point, Mumbai 400 097
              </p>
              <p>
                <FontAwesomeIcon icon="envelope" /> assistance@willandmore.com
              </p>
              <p>
                <FontAwesomeIcon icon="phone" /> +91 2235564357 / +91 7777076298
              </p>
            </div>
          </div>
        </div>
        <div className="footer__copyright">
          <div className="row">
            <div className="col-lg-8">
              <p>
                {" "}
                &copy; 2020 Will & More. All rights reserved.
                <NavLink to="/terms">Terms & Conditions</NavLink> |
                <NavLink to="/privacy-policy"> Privacy Policy</NavLink> |
                <NavLink to="/disclaimer"> Disclaimer</NavLink>
              </p>
            </div>
            <div className="col-lg-4">
              <div className="footer__copyright__text">
                <ul className="footer__copyright__links">
                  <li>
                    <a
                      target="_blank"
                      href="https://www.facebook.com/Will-And-More-101664185297890"
                      className="fa fa-facebook faicon"
                    ></a>
                    <a
                      target="_blank"
                      href="https://twitter.com/WillAndMore1"
                      className="fa fa-twitter faicon"
                    ></a>
                    <a
                      target="_blank"
                      href="https://www.instagram.com/willandmore1/"
                      className="fa fa-instagram faicon"
                    ></a>
                    <a
                      target="_blank"
                      href="https://www.linkedin.com/in/Will-and-more-62a4a1206/"
                      className="fa fa-linkedin faicon"
                    ></a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}
