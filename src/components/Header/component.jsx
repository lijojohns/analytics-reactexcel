import React from "react";
import logo from "../../assets/images/logo.png";

export default function Header(props) {
  return (
    <header className="header">
      <div className="header__top">
        <div className="container"></div>
      </div>
      <div className="container">
        <div className="row">
          <div className="col-lg-2">
            <div className="header__logo">
              <a href="/">
                <img src={logo} alt="Will & More" />
              </a>
            </div>
          </div>
          <div className="col-lg-10">
            <div className="header__nav">
              <nav className="navbar navbar-expand-lg header__menu">
                <ul>
                  <li>
                    <a href="#" to="/" activeclassname="active">
                      HOME
                    </a>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
}
