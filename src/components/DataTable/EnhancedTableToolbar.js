import Toolbar from "@material-ui/core/Toolbar";
import clsx from "clsx";
import { lighten, makeStyles } from "@material-ui/core/styles";
import { useState } from "react";

const useToolbarStyles = makeStyles((theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === "light"
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  title: {
    flex: "1 1 100%",
  },
}));

export function EnhancedTableToolbar(props) {
  const classes = useToolbarStyles();
  const { numSelected } = props;
  const [keyText, setKeyText] = useState("");

  function handleOnChange(e) {
    if (e.target && e.target.value) setKeyText(e.target.value);
  }

  function handleOnSubmit(e) {
    props.submit(keyText);
  }

  function handleOnDownload(e) {
    props.download();
  }

  return (
    <Toolbar
      className={clsx(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
      align="right"
    >
      <input
        type="text"
        name="keyText"
        className=""
        onChange={handleOnChange}
      />
      <input
        type="submit"
        className="edp-button"
        value="Submit"
        onClick={handleOnSubmit}
      />
      <input
        type="submit"
        className="edp-button"
        value="Download"
        onClick={handleOnDownload}
      />
    </Toolbar>
  );
}
