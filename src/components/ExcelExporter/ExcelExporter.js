import XLSX from "xlsx";
import saveAs from "save-as";

export default function ExcelExporter(rows, columns, sheetName) {
  var wb = XLSX.utils.book_new();
  var ws = XLSX.utils.json_to_sheet(rows, { header: columns });
  XLSX.utils.book_append_sheet(wb, ws, sheetName);

  var wbout = XLSX.write(wb, { bookType: "xlsx", type: "array" });
  saveAs(new Blob([wbout], { type: "application/octet-stream" }), "test.xlsx");
}
