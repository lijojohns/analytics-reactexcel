import React, { Component } from "react";
import XLSX from "xlsx";
import { SheetJSFT } from "./types";
import cloudUpload from "../../assets/images/cloud-upload.png";
import cloudUploadSuccess from "../../assets/images/cloud-upload-success.png";
import "./style.scss";
import generateColumns from "../../utils";

class ExcelReader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      file: {},
      data: [],
      cols: [],
      sheet: "",
    };
    this.format = {};
    this.handleFile = this.handleFile.bind(this);
    this.handleChange = this.handleChange.bind(this);
    // this.initTable();
  }

  handleChange(e) {
    const files = e.target.files;
    if (files && files[0]) this.setState({ file: files[0] });
  }

  handleFile() {
    /* Boilerplate to set up FileReader */
    const reader = new FileReader();
    const rABS = !!reader.readAsBinaryString;

    reader.onload = (e) => {
      /* Parse data */
      const bstr = e.target.result;
      const wb = XLSX.read(bstr, {
        type: rABS ? "binary" : "array",
        bookVBA: true,
      });

      // console.log(wb.SheetNames);
      const postArr = [];
      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];
      const data = XLSX.utils.sheet_to_json(ws);
      // console.log("sheet data", ws,data);
      this.setState(
        {
          data: data,
          cols: data.length > 0 ? generateColumns(data[0]) : [],
          sheet: wsname,
        },
        () => {
          // console.log(this.state);
          this.props.getData(this.state);
        }
      );
      let obj = {
        sheet_name: this.state.sheet,
        sheet_data: JSON.parse(JSON.stringify(this.state.data, null, 2)),
      };
      postArr.push(obj);
    };

    if (rABS) {
      reader.readAsBinaryString(this.state.file);
    } else {
      reader.readAsArrayBuffer(this.state.file);
    }
  }

  render() {
    // console.log(this.state.file);
    return (
      <div>
        <input
          type="file"
          id="file"
          accept={SheetJSFT}
          onChange={this.handleChange}
          hidden
        />
        {this.state.file.size != undefined ? (
          <div>
            <label>
              <img src={cloudUploadSuccess} className="cloud-upload" />
            </label>
            <p className="fileName">{this.state.file.name}</p>
          </div>
        ) : (
          <label htmlFor="file">
            <img src={cloudUpload} className="cloud-upload" />
          </label>
        )}
        <br />
        {this.state.file.size != undefined && (
          <input
            type="submit"
            className="edp-button"
            value="Generate"
            onClick={this.handleFile}
          />
        )}
      </div>
    );
  }
}

export default ExcelReader;
