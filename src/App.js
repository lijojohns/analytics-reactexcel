import Header from "./components/Header";
import "./App.css";
import DataTable from "./components/DataTable/component";

function App() {
  return (
    <div className="App">
      <Header />
      <div className="uploader">
        <DataTable />
      </div>
    </div>
  );
}

export default App;
